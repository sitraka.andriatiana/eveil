const {AccessVideo, Module, Module_AccessVideo} = require('../models');
const {QueryTypes} = require("sequelize");

module.exports = {
    async createAccessVideo(req, res) {
        if (req.method === 'POST') {
            const {email, password, nom, prenom, moduleId} = req.body;
            if (email && password && moduleId) {
                const moduleIdInt = parseInt(moduleId);
                if (!Number.isInteger(moduleIdInt)) {
                    return res.status(200).json({
                        status: 510, // Module non valide
                        email: email,
                        message: "KO"
                    });
                }
                const sql = `SELECT * FROM "AccessVideoModuleView" WHERE email = '${email}' AND "dateFin" > now()`;
                return await AccessVideo.sequelize
                    .query(sql, {
                        type: QueryTypes.SELECT,
                    })
                    .then(async accessVideo => {
                        if (accessVideo && accessVideo.length > 0) {
                            console.log("User Found");
                            const accessVideoId = accessVideo[0].AccessVideo_Id;
                            // Update nom et prénom si il y a changement
                            await AccessVideo.update({
                                nom: nom ? nom : accessVideo[0].nom,
                                prenom: prenom ? prenom : accessVideo[0].prenom
                            }, {
                                where: {
                                    id: accessVideoId,
                                },
                                returning: false
                            });

                            let modules = accessVideo.map(m => m.Module_Id);
                            if (modules.includes(moduleIdInt)) {
                                return res.status(200).json({
                                    status: 200, // A déja un compte et accès au module
                                    email: email,
                                    modules: modules,
                                    message: "OK"
                                });
                            }
                            return await Module.findOne({
                                where: {
                                    id: moduleIdInt
                                }
                            }).then(async module => {
                                if (module) {
                                    // Si le module existe
                                    Module_AccessVideo.removeAttribute('id');
                                    return await Module_AccessVideo.create({
                                        AccessVideo_Id: accessVideoId,
                                        Module_Id: module.id
                                    }).then(() => {
                                        return res.status(200).json({
                                            status: 210, // Module ajouté pour l'utilisateur
                                            email: email,
                                            modules: module.id,
                                            message: "OK"
                                        });
                                    });
                                }
                            });
                        } else {
                            console.log("Create User");
                            return await Module.findOne({
                                where: {
                                    id: moduleIdInt
                                }
                            }).then(async module => {
                                if (module) {
                                    return await AccessVideo.findOrCreate({
                                        where: {
                                            email: email
                                        },
                                        defaults: {
                                            email: email,
                                            password: password,
                                            nom: nom,
                                            prenom: prenom,
                                        }
                                    }).then(async ([accessVideo, created]) => {
                                        const accessVideoId = accessVideo.id;
                                        Module_AccessVideo.removeAttribute('id');
                                        return await Module_AccessVideo.create({
                                            AccessVideo_Id: accessVideoId,
                                            Module_Id: module.id
                                        }).then(() => {
                                            if (created) {
                                                return res.status(200).json({
                                                    status: 201, // AccesVideo créé & Module ajouté pour l'utilisateur
                                                    email: email,
                                                    message: "OK"
                                                });
                                            }
                                            return res.status(200).json({
                                                status: 210, // Module ajouté pour l'utilisateur
                                                email: email,
                                                modules: module.id,
                                                message: "OK"
                                            });
                                        });
                                    });
                                }
                                return res.status(200).json({
                                    status: 500, // Module non disponible
                                    email: email,
                                    message: "KO"
                                });
                            });
                        }
                    });
                }
            return res.json([email, password, nom, prenom]);
        }
        let email = req.params.email;
        let password = req.params.password;
        if (email && password) {
            const accessVideo = {
                email: email,
                password: password
            }
            await AccessVideo.findOrCreate({
                where: {
                    email: email
                },
                defaults: accessVideo
            }).then(([reponse, created]) => {
                if (created) {
                    return res.status(200).json("OK");
                } else {
                    return res.status(200).json("OK" + "/" + reponse.password);
                }
            });
        } else {
            return res.status(500).json("KO")
        }
    }
};
