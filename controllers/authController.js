const middleware = require('../services/middlewares');
module.exports = {
    login(req, res) {
        res.render('Authentification/login', {error: req.flash('error'), success: req.flash('success')});
    },
    logout(req, res) {
        delete req.session.userData;
        req.logout();
        res.redirect('/');
    },
};
