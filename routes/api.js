const express = require('express');
const router = express.Router();

const {createAccessVideo} = require('../controllers/apiController');

module.exports = (io) => {
    router.get('/:email/:password', createAccessVideo);
    router.post('/createUser', createAccessVideo)
    return router;
};
