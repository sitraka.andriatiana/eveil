const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Module_AccessVideo', {
    AccessVideo_Id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'AccessVideo',
        key: 'id'
      }
    },
    Module_Id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'Module',
        key: 'id'
      }
    },
    dateCreation: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.fn('now')
    }
  }, {
    sequelize,
    tableName: 'Module_AccessVideo',
    schema: 'public',
    timestamps: false
  });
};
