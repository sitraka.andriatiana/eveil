function addSpaceInNum(tel) {
  tel = tel.replace(/ /g, '');
  tel = tel.substring(0, 10);
  let indexSpace = [3, 5, 8];
  let spaced = '';
  let u = 0;
  for (let i = 0; i < tel.length; i++) {
    try {
      if (i == indexSpace[u]) {
        spaced = spaced + ' ' + tel[i];
        u++;
      } else {
        spaced = spaced + tel[i];
      }
    } catch (e) {
      console.log(e);
      break;
    }

  }
  return spaced;
}
