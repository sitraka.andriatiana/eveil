let nom = document.getElementById('nom');
let adresse = document.getElementById('adresse');
let invalidAdresse = document.getElementById('invalidAdresse');
let invalidNom = document.getElementById('invalidNom');
let email = document.getElementById("email");
let tel = document.getElementById("tel");
invalidTel = document.getElementById("invalidTel2");
let btnValider = document.getElementById("valider");

let isNomValide = false;
let isAdresseValide = false;
let isEmailValide = false;
let isTelValide = true;

let urlListeActeur = '/acteur';
let urlCreerActeur = '/acteur/creation';
let urlIsNomValide = '/acteur/isNomValide';

function isBlank(idElt){
    let elt = document.getElementById(idElt);
    let noSpace = elt.value.replace(/ /g, '');
    if(noSpace == ''){
        return true;
    }
    return false;
}

function printError(idDivError){
    document.getElementById(idDivError).style.borderColor = "red";
}
function printOK(idDivError){
    document.getElementById(idDivError).style.borderColor = "green";
}

function controlError(idEltInput, isEltValid, idDivError){
    let isEltInputBlank = isBlank(idEltInput);
    let error = document.getElementById(idDivError);
    if(isEltInputBlank){
        isEltInputBlank = false;
        error.style.display = "block"
        printError(idDivError);
    }else{
        isEltInputBlank = true;
        error.style.display = "none";
        printOK(idDivError);
    }
}

function controlBlankValues(){
    controlError("nom", isNomValide, "invalidNom");
    controlError("adresse", isAdresseValide, "invalidAdresse");
    //controlError("num", isTelValide, "invalidTel");
    controlError("email", isEmailValide, "invalidEmail");
}

function validateTelBase(num){
    let url = '/acteur/isTelValide?num=' + num;
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onload = function(e){
        console.l
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200){
            isTelValide = JSON.parse(xhr.response).isValide;
            console.log(JSON.parse(xhr.response).isValide);
            if(isTelValide){
                invalidTel.style.display = "none";
                creerActeur(num);
            }else{
                invalidTel.style.display = "block";
            }
        }
    };
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }
    xhr.send();
}

function creerActeur(numero){
    validateEmail();
    if(isEmailValide){
        invalidEmail.style.display = "none";
        printOK("invalidEmail");
    }else{
        invalidEmail.style.display = "block";
        printError("invalidEmail");
    }
    if(!isNomValide || !isEmailValide || !isTelValide){
        return 0;
    }

    const param_nom = nom.value;
    const param_adresse = adresse.value;
    const param_email = email.value;
    const param_numero = numero + ' ';

    console.log('num azo = ' + numero);
    //isTelValide = validateTel(param_numero);
    if(isTelValide){
        invalidTel.style.display = "none";
    }else{
        invalidTel.style.display = "block";
    }

    let params = {nom : param_nom, adresse : param_adresse, tel : param_numero, email : param_email};

    let xhr = new XMLHttpRequest();
    xhr.open("POST", urlCreerActeur, true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.onload = function(e){
        if(xhr.readyState === XMLHttpRequest.DONE){
            window.location.replace(urlListeActeur);
        }
        window.location.replace(urlListeActeur);
    };
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }
    xhr.send(JSON.stringify(params));
}

function validateNom(nom){
    let url = urlIsNomValide + '?nom=' + nom;
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onload = function(e){
        console.l
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200){
            isNomValide = JSON.parse(xhr.response).isValide;
            console.log(JSON.parse(xhr.response).isValide);
            if(isNomValide){
                invalidNom.style.display = "none";
            }else{
                invalidNom.style.display = "block";
            }
        }
    };
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }
    xhr.send();
}

function addSpaceInNum(tel){
    tel = tel.replace(/ /g, '');
    tel = tel.substring(0, 10);
    let indexSpace = [3, 5, 8];
    let spaced = '';
    let u = 0;
    for(let i = 0; i < tel.length; i++){
        try{
            if(i == indexSpace[u]){
                spaced = spaced +  ' ' + tel[i];
                u++;
            }else{
                spaced = spaced + tel[i];
            }
        }catch(e){
            console.log(e);
            break;
        }

    }
    return spaced
}

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function validateTel(tel){
    try{
        tel = tel.replace(/ /g, '');
        if(tel.length != 10){
            return false;
        }
        let digits = '0123456789';
        let operateur = tel[0] + tel[1] + tel[2];
        let operateurs = ['020', '032', '033', '034'];
        let operateurOK = false;
        for(let i = 0; i < operateurs.length; i++){
            if(operateur == operateurs[i]){
                operateurOK = true;
            }
        }
        if(!operateurOK){
            printError("invalidTel");
            return false;
        }
        for(let i = 3; i < tel.length; i++){
            if(!digits.includes(tel[i])){
                return false;
            }
        }
        printOK("invalidTel");
        return true;
    }catch(e){
        //console.log(e);
        return false;
    }
}

function validateTel2(tel){
    tel = tel.replace(/ /g, '');
    let re = new RegExp('^[0-9]{10}$');
    if(re.test(tel)){
        return true;
    }
    return false;
}

nom.addEventListener("change", function(){
    isNomValide = validateNom(nom.value);
});


adresse.addEventListener("input", function(){
    controlError("adresse", isAdresseValide, "invalidAdresse");
});

email.addEventListener("input", function(){
    isEmailValide = validateEmail(email.value);
    if(isEmailValide){
        invalidEmail.style.display = "none";
        printOK("invalidEmail");
    }else{
        invalidEmail.style.display = "block";
        printError("invalidEmail");
    }
});



tel.addEventListener("input", event =>{
    tel.value = tel.value.replace(/_/g, '');
    tel.value = addSpaceInNum(tel.value);
    isTelValide = validateTel(tel.value);
    //applyMaskToNum("num");
    if(isTelValide){
        invalidTel.style.display = "none";
        element.classList.add("mystyle");
        printOK("invalidTel");
    }else{
        invalidTel.style.display = "block";
        printError("invalidTel");
    }
});

valider.addEventListener("click", function(){
    controlBlankValues();
    validateTelBase(String(tel.value));
});